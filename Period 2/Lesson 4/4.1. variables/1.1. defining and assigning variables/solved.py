# Digər dillərdən fərqli olaraq Python dilində dəyişənlərin tipi qeyd olunmur. Bir başa dəyişənin adı yazılır. 
# Ona mənimsədilən dəyərin tipindən asılı olaraq avtomatik dəyişənin tipi təyin edilir.
# Əvvəlcə dəyişənin adı yazılır, daha sonra bərabərlik qoyulur və bərabərliyin sol tərəfində 
# dəyişənin dəyəri yazılır.
deyishen_1 = 10
print("value of deyishen_1 variable is:", deyishen_1)
print("type of deyishen_1 variable is:", type(deyishen_1))

deyishen_2 = "salam dünya"
print("value of deyishen_2 variable is:", deyishen_2)
print("type of deyishen_2 variable is:", type(deyishen_2))

# Bir dəyişən digərinə mənimsədilə bilər. Bu zaman hər iki dəyişənin dəyəri və tipi eyni olacaq.
deyishen_3 = 20
deyishen_4 = deyishen_3
print("value of deyishen_3 is:", deyishen_3, ". type is:", type(deyishen_3))
print("value of deyishen_4 is:", deyishen_4, ". type is:", type(deyishen_4))

# Bir birinə mənimsədilmiş dəyişənin dəyəri digərinə kopyalanmır, sadəcə hər iki dəyişən 
# yaddaşın eyni xanasına yönləndirilmiş olur. Bu telefon kitabçasında eyni nömrənin 
# iki fərqli ad üçün yazılmasına bənzəyir.
print("address of deyishen_3 variable is:", id(deyishen_3))
print("address of deyishen_4 variable is:", id(deyishen_4))

# Bir sətirdə nöqtə-vergül ilə bir birindən ayrılaraq birdən artıq dəyişən təyin edilə bilər.
deyishen_5 = 22; deyishen_6 = 16.5; deyishen_7 = "Arif"
print("The variables defined in one line:", deyishen_5, deyishen_6, deyishen_7)

# Bir neçə dəyişənə eyni anda eyni dəyəri mənimsətmək mümkündür.
deyishen_8 = deyishen_9 = deyishen_10 = 33
print("value of deyishen_8 is:", deyishen_8, ". type is:", deyishen_8, ". address is:", id(deyishen_8))
print("value of deyishen_9 is:", deyishen_9, ". type is:", deyishen_9, ". address is:", id(deyishen_9))
print("value of deyishen_10 is:", deyishen_10, ". type is:", deyishen_10, ". address is:", id(deyishen_10))