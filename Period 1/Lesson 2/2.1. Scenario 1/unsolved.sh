# ISTIFADEDCHI adlı dəyişən yaradın və whoami komandasının köməkliyi ilə istifadəçinin adını bu dəyişənə mənimsədin

# OS adlı dəyişən yaradın və bu dəyişənə uname komandasının vasitəsi ilə əməliyyat sisteminin növü və kernelin versiyasını mənimsədin

# ISTIFADEDCHI və OS dəyişənlərinin birləşməsindən əmələ gələn sözləri DIRNAME dəyişəninə mənimsədin, istifadəçinin home folderində eyni adlı direktoriya yaradın

# daxil olduğunuz istifadəçi adından run olan bütün proseslərin siyahısını hər hansı bir fayla yazın və onu $DIRNAME folderinə saxlayın

# $DIRNAME folderində 10 ədəd direktoriya yaradın və hər birinin içərisində 100 ədəd .txt fayl yaradın

# find ilə $DIRNAME folderindəki sonu 2.txt ilə bitən bütün faylların siyahısını a-dan z-yə sıralanmış şəkildə fayla yazın

# $DIRNAME folderindəki əvvəli file_ başlayan, sonu .txt ilə bitən faylları və onları daşıyan folderləri silin